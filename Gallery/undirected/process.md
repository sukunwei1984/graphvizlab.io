---
redirect_from:
  - /_pages/Gallery/undirected/process.html
layout: gallery
title: Process
svg: process.svg
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: process.gv.txt
img_src: process.png
---
This graph was created from a hand-made figure
in an operating system paper.
